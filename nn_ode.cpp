#include <vector>
#include <fstream>
#include <sstream>
#include <exception>

#include <vexcl/vexcl.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/external/vexcl/vexcl.hpp>
#include <boost/program_options.hpp>

//---------------------------------------------------------------------------
typedef vex::multivector<double, 14> state_type;

enum VarNames {
    V_DEND  ,  // dend.V_dend
    V_SOMA  ,  // soma.V_soma
    V_AXON  ,  // axon.V_axon
    R_D     ,  // dend.Calcium_r
    Z_D     ,  // dend.Potassium_s
    N_D     ,  // dend.Hcurrent_q
    CA_CONC ,  // dend.Ca2Plus
    K_S     ,  // soma.Calcium_k
    L_S     ,  // soma.Calcium_l
    H_S     ,  // soma.Sodium_h
    N_S     ,  // soma.Potassium_n
    X_S     ,  // soma.Potassium_x_s
    H_A     ,  // axon.Sodium_h_a
    X_A        // axon.Potassium_x_a
};

struct nn_system {
    double Cmd = 1;
    double Cms = 1;
    double Cma = 1;

    double g_int = 0.13;
    double g_cah_d = 4.5;
    double g_kca_d = 35;
    double g_h_d = 0.125;
    double g_leak_d = 0.016;
    double g_cal_s = 0.68;
    double g_na_s = 150;
    double g_kdr_s = 9;
    double g_k_s = 5;
    double g_leak_s = 0.016;
    double g_na_a = 240;
    double g_k_a = 20;
    double g_leak_a = 0.016;

    double p1 = 0.25;
    double p2 = 0.15;

    double Vcah_d = 120;
    double Vkca_d = -75;
    double Vh_d = -43;
    double Vleak_d = 10;
    double Vcal_s = 120;
    double Vna_s = 55;
    double Vkdr_s = -75;
    double Vk_s = -75;
    double Vleak_s = 10;
    double Vna_a = 55;
    double Vk_a = -75;
    double Vleak_a = 10;

    const vex::vector<int> &ptr, &col;
    const vex::vector<double> &w;

    nn_system(
            const vex::vector<int>    &ptr,
            const vex::vector<int>    &col,
            const vex::vector<double> &w
            ) : ptr(ptr), col(col), w(w)
    {}

    void operator()(const state_type &s, state_type &dsdt, double t) const {
        using namespace vex;

        int n = s.size();

        double Iapp = 0;

        auto v_dend  = tag<V_DEND >(s(V_DEND));
        auto v_soma  = tag<V_SOMA >(s(V_SOMA));
        auto v_axon  = tag<V_AXON >(s(V_AXON));
        auto r_d     = tag<R_D    >(s(R_D));
        auto z_d     = tag<Z_D    >(s(Z_D));
        auto n_d     = tag<N_D    >(s(N_D));
        auto ca_conc = tag<CA_CONC>(s(CA_CONC));
        auto k_s     = tag<K_S    >(s(K_S));
        auto l_s     = tag<L_S    >(s(L_S));
        auto h_s     = tag<H_S    >(s(H_S));
        auto n_s     = tag<N_S    >(s(N_S));
        auto x_s     = tag<X_S    >(s(X_S));
        auto h_a     = tag<H_A    >(s(H_A));
        auto x_a     = tag<X_A    >(s(X_A));

        VEX_FUNCTION(double, get_Igap, (int, i)(int*, ptr)(int*, col)(double*, w)(double*, v_dend),
                int row_beg = ptr[i];
                int row_end = ptr[i+1];
                double s = 0;
                double v_i = v_dend[i];
                for(int j = row_beg; j < row_end; ++j) {
                    double v_ij = v_i - v_dend[col[j]];
                    s += w[j] * (0.8 * exp(-1e-2 * v_ij * v_ij) + 0.2) * v_ij;
                }
                return s;
                );

        auto Igap    = get_Igap(element_index(0, n), raw_pointer(ptr), raw_pointer(col), raw_pointer(w), raw_pointer(s(V_DEND)));
        auto Isd     = g_int / (1 - p1) * (v_dend - v_soma);
        auto Icah_d  = -g_cah_d * r_d * r_d * (Vcah_d - v_dend);
        auto Ikca_d  = -g_kca_d * z_d * (Vkca_d - v_dend);
        auto Ih_d    = -g_h_d * n_d * (Vh_d - v_dend);
        auto Ileak_d = -g_leak_d * (Vleak_d - v_dend);

        auto Ids     = g_int / p1 * (v_soma - v_dend);
        auto Ias     = g_int / (1 - p2) * (v_soma - v_axon);
        auto Ical_s  = -g_cal_s * k_s * k_s * k_s * l_s * (Vcal_s - v_soma);
        auto m_s     = make_temp<1>(1 / (1 + exp(-(v_soma + 30) / 5.5)));
        auto Ina_s   = -g_na_s * m_s * m_s * m_s * h_s * (Vna_s - v_soma);
        auto Ikdr_s  = -g_kdr_s * n_s * n_s * n_s * n_s * (Vkdr_s - v_soma);
        auto Ik_s    = -g_k_s * x_s * x_s * x_s * x_s * (Vk_s - v_soma);
        auto Ileak_s = -g_leak_s * (Vleak_s - v_soma);

        auto Isa     = g_int / p2 * (v_axon - v_soma);
        auto m_a     = make_temp<2>(1 / (1 + exp(-(v_axon + 30)/5.5)));
        auto Ina_a   = -g_na_a * m_a * m_a * m_a * h_a * (Vna_a - v_axon);
        auto Ik_a    = -g_k_a * x_a * x_a * x_a * x_a * (Vk_a - v_axon);
        auto Ileak_a = -g_leak_a * (Vleak_a - v_axon);

        dsdt = std::make_tuple(
                /*v_dend */ (-Igap + Iapp - Isd - Icah_d - Ikca_d - Ih_d - Ileak_d) / Cmd,

                /*v_soma */ (-Ids - Ias - Ical_s - Ina_s - Ikdr_s - Ik_s - Ileak_s) / Cms,

                /*v_axon */ (-Isa - Ina_a - Ik_a - Ileak_a) / Cma,

                /*r_d    */ 0.2 * 1.7 / (1 + exp(-(v_dend - 5)/13.9)) * (1 - r_d) -
                            0.2 * 0.1 * (v_dend + 8.5) / (-5) * r_d / (1 - exp((v_dend + 8.5)/5)),

                /*z_d    */ min(2e-5 * ca_conc, 1e-2) * (1 - z_d) - 0.015 * z_d,

                /*n_d    */ (1 / (1 + exp((v_dend + 80) / 4)) - n_d) *
                            (exp(-0.086 * v_dend - 14.6) + exp(0.070 * v_dend - 1.87)),

                /*ca_conc*/ -3 * Icah_d - 0.075 * ca_conc,

                /*k_s    */ 1 / (1 + exp(-(v_soma + 61)/4.2)) - k_s,

                /*l_s    */ (1 / (1 + exp((v_soma + 85.5) / 8.5)) - l_s) /
                            ((20 * exp((v_soma + 160) / 30) / (1 + exp((v_soma + 84)/7.3))) + 35),

                /*h_s    */ (1 / (1 + exp((v_soma + 70) / 5.8)) - h_s) /
                            (3 * exp(-(v_soma + 40) / 33)),

                /*n_s    */ (1 / (1 + exp(-(v_soma + 3) / 10)) - n_s) /
                            (5 + 47 * exp((v_soma + 50) / 900)),

                /*x_s    */ (1 - x_s) * (0.13 * (v_soma + 25)) / (1 - exp(-(v_soma + 25) / 10)) -
                            x_s * 1.69 * exp(-(v_soma + 35) / 80),

                /*h_a    */ (1 / (1 + exp((v_axon + 60) / 5.8)) - h_a) /
                            (1.5 * exp(-(v_axon + 40) / 33)),

                /*x_a    */ (1 - x_a) * (0.13 * (v_axon + 25)) / (1 - exp(-(v_axon + 25) / 10)) -
                            x_a * 1.69 * exp(-(v_axon + 35) / 80)
                );
    }
};

//---------------------------------------------------------------------------
void read_conn(std::string fname,
        int n,
        std::vector<int> &ptr,
        std::vector<int> &col,
        std::vector<double> &w
        )
{
#if 0
    std::ifstream f(fname);
    if (!f) throw std::runtime_error("wrong conn file?");

    ptr.clear(); ptr.reserve(1024); ptr.push_back(0);
    col.clear(); col.reserve(1024);
    w.clear(); w.reserve(1024);

    std::string line;
    for(int i = 0; i < n; ++i) {
        std::getline(f, line);
        if (!f) throw std::runtime_error("file format error (out of lines)");

        std::istringstream s(line);

        for(int j = 0; j < n; ++j) {
            double v;
            s >> v;
            if (v != 0) {
                col.push_back(j);
                w.push_back(v);
            }
        }
        if (!s) throw std::runtime_error("file format error (out of columns)");

        ptr.push_back(col.size());
    }
#else
    ptr.resize(n+1,0);
    col.resize(1,0);
    w.resize(1,0);
#endif
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    namespace po = boost::program_options;
    po::options_description desc("Options");

    desc.add_options()
        ("help,h", "Show this help.")
        ("size,n",   po::value<int>()->default_value(1000),                 "number of cells to use")
        ("conn,f",   po::value<std::string>()->default_value("conn.txt"),   "connectivity file")
        ("tmax",     po::value<double>()->default_value(10.0, "10.0"),      "tmax")
        ("tau",      po::value<double>()->default_value(0.05, "0.05"),      "time step")
        ("wstep,w",  po::value<double>()->default_value(0.1,  "0.1"),       "period for writing results")
        ("output,o", po::value<std::string>()->default_value("output.txt"), "output file")
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    int         n         = vm["size"].as<int>();
    std::string conn_file = vm["conn"].as<std::string>();
    std::string out_file  = vm["output"].as<std::string>();
    double      tmax      = vm["tmax"].as<double>();
    double      dt        = vm["tau"].as<double>();
    double      wstep     = vm["wstep"].as<double>();

    namespace odeint = boost::numeric::odeint;

    vex::Context ctx(vex::Filter::Env && vex::Filter::Count(1));
    std::cout << ctx << std::endl;

    vex::profiler<> prof(ctx);

    std::vector<int> _ptr, _col;
    std::vector<double> _w;

    read_conn(conn_file, n, _ptr, _col, _w);

    vex::vector<int>    ptr(ctx, _ptr);
    vex::vector<int>    col(ctx, _col);
    vex::vector<double> w  (ctx, _w);

    nn_system NN(ptr, col, w);

    odeint::euler/*runge_kutta4_classic*/<
        state_type, double, state_type, double,
        odeint::vector_space_algebra, odeint::default_operations
        > stepper;

    state_type y(ctx, n);

    y(V_DEND)  = -60;
    y(V_SOMA)  = -60;
    y(V_AXON)  = -60;
    y(R_D)     = 0.0112788;
    y(Z_D)     = 0.0049291;
    y(N_D)     = 0.0337836;
    y(CA_CONC) = 3.7152;
    y(K_S)     = 0.7423159;
    y(L_S)     = 0.0321349;
    y(H_S)     = 0.3596066;
    y(N_S)     = 0.2369847;
    y(X_S)     = 0.1;
    y(H_A)     = 0.9;
    y(X_A)     = 0.2369847;

    std::ofstream out(out_file);
    prof.tic_cl("integrate");
#if 1
    int step = 0;
    double chk_point = wstep;
    for(double t = 0; t < tmax; t += dt, ++step) {
        stepper.do_step(std::ref(NN), y, t, dt);
        if (t >= chk_point) {
            chk_point += wstep;

            out << t;
            auto v_axon = y(V_AXON).map(0);
            for(int i = 0; i < n; ++i) out << " " << v_axon[i];
            out << std::endl;
        }
    }
#else
    stepper.do_step(std::ref(NN), y, 0.0, dt);

#define REPORT(vname) \
    std::cout << #vname << ":\t" << std::scientific << std::setprecision(16) \
              << y(vname)[0] << std::endl

    REPORT(V_DEND);
    REPORT(V_SOMA);
    REPORT(V_AXON);
    REPORT(R_D);
    REPORT(Z_D);
    REPORT(N_D);
    REPORT(CA_CONC);
    REPORT(K_S);
    REPORT(L_S);
    REPORT(H_S);
    REPORT(N_S);
    REPORT(X_S);
    REPORT(H_A);
    REPORT(X_A);
#endif
    prof.toc("integrate");

    std::cout << prof << std::endl;
}
